from django.conf import settings
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()


handler500 = "pinax.views.server_error"


urlpatterns = patterns("",
    url(r"^$", 'wook.views.index', name="index"),
    url(r"^admin/", include(admin.site.urls)),
    url(r'^openid/', include('django_openid_auth.urls')),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),

    url(r"^soldiers/", "wook.views.view_soldiers", name="view_soldiers"),
    url(r"^soldier/add/", "wook.views.add_soldier", name="add_soldier"),
    url(r"^recent/", "wook.views.recent_added", name="recent_soldiers"),
    url(r"^privacy/", direct_to_template, {"template": "privacy.html"}),
    url(r"^failure/", direct_to_template, {"template": "failure.html"}),
    url(r"^list/", "wook.views.list_goons", name="goon_list"),
)


if settings.SERVE_MEDIA:
    urlpatterns += patterns("",
        url(r"", include("staticfiles.urls")),
    )
