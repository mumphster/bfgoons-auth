from django.db import models
from django.contrib.auth.models import User
from django_openid_auth.models import UserOpenID
from django.conf import settings
from steamtools import get_info
from django.utils.html import escape
from django.http import HttpResponse
from django.db.models.signals import post_save, post_delete
from django.shortcuts import redirect
from django.contrib import messages


class SteamUser(models.Model):
    user = models.ForeignKey(User)
    steam_id = models.CharField(max_length=500)
    steam_name = models.CharField(max_length=500)
    steam_url = models.URLField(max_length=400)
    avatar_small = models.URLField(max_length=400)
    avatar_medium = models.URLField(max_length=400)
    avatar_large = models.URLField(max_length=400)
   
    def __unicode__(self):
        return self.steam_name


class BattlelogUser(models.Model):
    steam_user = models.ForeignKey(SteamUser)
    user_name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.user_name


def create_steam_user(sender, instance, created, **kwargs):
    if created:
        steam_id = instance.claimed_id
        s = get_info(steam_id, settings.STEAM_API_KEY)

        if set(settings.STEAM_GROUPS) & set(s['user_groups']):
            in_group = True
        else:
            in_group = False

        print in_group
        print settings.STEAM_GROUPS
        print s['user_groups']
        print s['id']

        if in_group:

            steamuser = SteamUser()
            steamuser.user = instance.user
            steamuser.steam_id = s['id']
            steamuser.steam_name = s['username']
            steamuser.avatar_small = s['av_small']
            steamuser.avatar_medium = s['av_medium']
            steamuser.avatar_large = s['av_large']
            steamuser.steam_url = s['url']
        
            steamuser.save()

        else:
            user = instance.user
            instance.delete()
            user.delete()

def delete_steam_user(sender, instance, created, **kwargs):
    steam_user = SteamUser.objects.get(user=instance)
    steam_user.delete()

post_save.connect(create_steam_user, sender=UserOpenID)
#post_delete.connect(delete_steam_user, sender=User)
