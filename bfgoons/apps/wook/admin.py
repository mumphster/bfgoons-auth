from django.contrib import admin
from wook.models import SteamUser, BattlelogUser


admin.site.register(SteamUser)
admin.site.register(BattlelogUser)
