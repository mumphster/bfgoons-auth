from django.forms import ModelForm
from wook.models import BattlelogUser


class AddSoldier(ModelForm):
    class Meta:
        model = BattlelogUser
        exclude = ('steam_user',)

