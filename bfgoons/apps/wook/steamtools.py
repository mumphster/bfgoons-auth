import requests
import json
from utils import http
from django.conf import settings


#bfgoons - 103582791430613250
def get_info(url, api_key):
    steam_id = url.split('/')[-1]
    api_info = http.get_json('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%s' % (api_key, steam_id))

    username = api_info['response']['players'][0]['personaname']
    av_small = api_info['response']['players'][0]['avatar']
    av_medium = api_info['response']['players'][0]['avatarmedium']
    av_large = api_info['response']['players'][0]['avatarfull']
    user_url = 'http://steamcommunity.com/profiles/%s' % steam_id

    user_xml = http.get_xml(user_url + '?xml=1')
    user_groups = user_xml.xpath('//profile/groups/group/groupID64/text()')

    return {'id': steam_id, 'username': username, 'user_groups': user_groups, 'av_small': av_small, 'av_medium': av_medium, 'av_large': av_large, 'url': user_url}


if __name__ == '__main__':
    x = get_info('http://steamcommunity.com/openid/id/76561197965920264', 'DA03E615A97A09C856772E564D56F3AD')
    print x['username']
    print x['av_small']
    print x['av_large']
    print x['url']

