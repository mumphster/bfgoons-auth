from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.utils.html import escape
from django_openid_auth.models import UserOpenID
from wook.models import SteamUser, BattlelogUser
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from wook.forms import AddSoldier


def steam_context(request):
    if request.user.is_authenticated():
        if request.user.id == 1:
            return {'steam_user': ''}
        else:
            steam_user = SteamUser.objects.get(user=request.user)
            return {'steam_user': steam_user}
    elif request.user.is_anonymous():
        return {'steam_user': ''}

def index(request):
    if request.user.is_authenticated():
        return render(request, 'homepage_authd.html', {})
    else:
        return render(request, 'homepage.html', {})

@login_required
def view_soldiers(request):
    steam_user = SteamUser.objects.get(user=request.user)
    return render(request, 'soldiers.html', {"soldiers": steam_user.battleloguser_set.all()})

@login_required
def add_soldier(request):
    if request.method == 'POST':
        form = AddSoldier(request.POST)
        if form.is_valid():
            steamuser = SteamUser.objects.get(user=request.user)
            username = form.cleaned_data['user_name']
            b = BattlelogUser()
            b.steam_user = steamuser
            b.user_name = username
            b.save()
            return redirect('/soldiers/')
    else:
        form = AddSoldier()

    return render(request, 'add_soldier.html', {'form': form})
            
@login_required
def recent_added(request):
    recent = BattlelogUser.objects.order_by('-id')[:10]
    return render(request, 'recent.html', {'recent': recent})

def list_goons(request):
    b = BattlelogUser.objects.all()
    return render(request, 'list.html', {'list': b})
